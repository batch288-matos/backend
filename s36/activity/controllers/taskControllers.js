const Task = require('../models/task.js');

// Controllers

// This controller will get/retrieve all the document from the tasks collections

module.exports.getAllTasks = (request, response) => {
	Task.find({})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
};

// Create a controller that will a data in our database
module.exports.addTasks = (request, response) => {
	Task.findOne({
		'name': request.body.name
	})
	.then(result => {
		if(result !== null){
			return response.send('Duplicate task');
		}
		else{
			let newTask = new Task({
				'name': request.body.name
			})

			newTask.save();

			return response.send('New task created!');
		};
	}).catch(error => response.send(error));
};

module.exports.deleteTask = (request, response) => {
	console.log(request.params.id);

	// In mongoose, we have the findByIdAndRemove method that will look for a document with the same id provided from the URL and remove/delete the document from the MongoDB
	Task.findByIdAndRemove(request.params.id)
	.then(result => {
		return response.send(`The document that has the _id of ${request.params.id} has been deleted`)
	})
	.catch(error => response.send(error));
};

module.exports.getTask = (request, response) => {
	Task.findById(request.params.id)
	.then(result => {
		return response.send(result);
	})
	.catch(error => response.send(error));
};


module.exports.updateTask = (request, response) => {
	Task.findByIdAndUpdate(request.params.id, {status: 'complete'}, {new: true})
	.then(result => {
		return response.send(result);
	})
	.catch(error => response.send(error));
};