const Users = require('../models/users.js');
const Courses = require('../models/courses.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');
// Controllers

// Create a controller for the signup
// registerUser
// Business Logic/Flow
	// 1. We have to validate whether the user is existing or not by validating whether the email exist on our databases or not
	// 2. If the user email is existing, we will prompt an error telling the user that the email is taken.
	// 3. Otherwise, we will sign up or add the user in our database
module.exports.registerUser = (request, response) => {
	// find method: it will return the an array of object that fits the given criteria
	Users.findOne({email: request.body.email})
	.then(result => {
		// We need to add if statement to verify whether the email exist already in our database
		if(result){
			return response.send(false);
		}
		else{
			// Create a new Object instantiated using the Users Model
			let newUser = new Users({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				// The hashSync() method: it hash/encrypt our password
				// The second argument salt rounds
				password: bcrypt.hashSync(request.body.password, 10),
				mobileNumber: request.body.mobileNumber
			});

			// Save the user
			// Error handling
			newUser.save()
			.then(save => response.send(true))
			.catch(error => response.send(false));
		};
	})
	.catch(error => response.send(false));
};

// New controller for the authentication of the user
module.exports.loginUser = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {
		if(!result){
			return response.send(false);
		}
		else{
			// The compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieve from the find method returns true or false depending the result of comparison
			// const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(bcrypt.compareSync(request.body.password, result.password)){
				return response.send({auth: auth.createAccessToken(result)}
				);
			}
			else{
				return response.send(false);
			}
		};
	})
	.catch(error => response.send(false));
};

// Retrieving all users
module.exports.getAllProfiles = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Users.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
	else{
		return response.send("You don't have access to this route!");
	}
	// .catch(error => response.send(error));
};

// Retrieving user details

module.exports.getProfile = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	// console.log(userData);

	// if(auth.decode(request.headers.authorization).isAdmin){
	if(userData.isAdmin){
		// Users.findById(request.body._id)
		Users.findOne({_id: request.body._id})
		.then(result => {
			result.password = '';
			return response.send(result);
		})
		.catch(error => response.send(error));
	}
	else{
		return response.send("You are not an admin, you don't have access to this route");
	};
};


// Controller for enrolling course
module.exports.enrollCourse = (request, response) => {
	const courseId = request.body.id;
	const userData = auth.decode(request.headers.authorization);
	// console.log(courseId);
	if(userData.isAdmin){
		return response.send(false);
	}
	else{
		// Push to user document
		let isUserUpdated = Users.findOne({_id: userData.id})

		.then(result => {
			result.enrollments.push({
				courseId: courseId
			});

			result.save()
			.then(saved => true)
			.catch(error => false)
		})
		.catch(error => false);
		// Push to course document
		let isCourseUpdated = Courses.findOne({_id: courseId})
		.then(result => {
			result.enrollees.push({
				userId: userData.id
			});

			result.save()
			.then(saved => true)
			.catch(error => false)
		})
		.catch(error => false);

		// If condition to check wether we updated the users document and courses document
		if(isUserUpdated && isCourseUpdated){
			return response.send(true);
		}
		else{
			return response.send(false);
		};
	};
};

module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	Users.findOne({_id: userData.id})
	.then(data => response.send(data));
}