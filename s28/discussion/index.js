// show databases - list of the db inside our cluster
// use "dbName" - to use a specific database
// show collections - to see the list of collections inside the db


// CRUD operation
/*
	- CRUD opereation is the heart of any backend application
	- mastering the CRUD operation is essential for any developer especially to those who want to become backend developer
*/

// [Section] Inserting Document (Create)
	// Insert one document
		/*
			Since mongoDB deals with objects as it's structure for documents we can easily create them by providing objects in our method/operation

			Syntax:
				db.collectionName.insertOne({
					object
				})
		*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Phyton"],
	department: "none"
})

// Insert Many

/*
	Syntax:
		db.collectionName.insertMany([{objectA}]), ([{objectB}]);
*/

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87654321",
			emails: "stephenhawking@gmail.com"
		},
		courses: ["Phyton", "React", "PHP"],
		department: "none"
	},

	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			emails: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}
])

db.userss.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Phyton"],
	department: "none"
})

// [Section] Finding Documents (Read operation)
	// db.collectionName.find();
	// db.collectionName.find({field:value});
// Using the find() method, it will show you the list of all the documents inside our collection
db.users.find();
// The "pretty" methods allows us to be able to view the documents returned by or terminals to be in a better format
db.users.find().pretty();
// It will return the documents that will pass the criteria given in the method
db.users.find({firstName: "Stephen"});

// [Section] Updating documents (Update)
db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "000000000",
		email: "test@gmail.com"
	},
	course: [],
	department: "none"
})

// UpdateOne Method
/*
	Syntax: 
		db.collectionName.updateOne({criteria, {$set: {field:value}}});
*/

db.users.updateOne(
		{firstName: "test"},
		{
			$set: {
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact: {
					phone: "12345678",
					email: "billgates@gmail.com"
				},
				course: ["PHP", "Laravel", "HTML"],
				department: "Operations",
				status: "none"
			}
		}
	)

db.users.updateOne(
		{firstName: "Bill"},
		{
			$set: {
				firstName: "Chris"
			}
		}
	)

db.users.updateOne(
		{firstName: "Jane"},
		{
			$set: {
				lastName: "Edited"
			}
		}
	)

// Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany(
			{criteria},
			{
				$set: {
					field:value
				}
			}
		)
*/

db.users.updateMany(
		{department: "none"},
		{
			$set: {
				department: "HR"
			}
		}
	)

// Replace One
/*
	Syntax:
		db.collectionName.replaceOne(
			{criteria},
			{
				Object	
			}
		)
*/

db.users.insertOne({firstName: "test"});

db.users.replaceOne(
		{firstName: "test"},
		{
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact: {},
				courses: [],
				department: "Operations"
		}
	)

// [Section] Deleting Documents
//  Deleting single document
/*
	db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({firstName: "Bill"});

// Deleting multiple documents
/*
	db.collectionName.deleteMany({criteria})
*/

db.users.deleteMany({firstName: "Jane"});
// All the documents in our collection will be deleted
db.users.deleteMany({});