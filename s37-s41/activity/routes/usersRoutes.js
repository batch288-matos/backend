const express = require('express');
const usersControllers = require('../controllers/usersControllers.js');
const auth =  require('../auth.js');


const router = express.Router();

// Routes

// registration
router.post('/register', usersControllers.registerUser);
// login
router.post('/login', usersControllers.loginUser);

// get all users
router.get('/', auth.verify, usersControllers.getAllProfiles);

router.get('/userDetails', auth.verify, usersControllers.retrieveUserDetails);

// user details
router.get('/details', auth.verify, usersControllers.getProfile);

// router for course enrollment
router.post('/enroll', auth.verify, usersControllers.enrollCourse);

// token verification
// router.get('/verify', auth.verify);

module.exports = router;