// To check whether the script file is properly linked to your html file.
/*console.log("Hello!");*/

// [Section] Arithmetic Operators
let x = 1397;
let y = 7831;
	// Addition Operator (+)
	let sum = x + y;
	console.log("Result of addition operator: " + sum);
	// Substraction Operation (-)
	let difference = y - x;
	console.log("Result of substraction operation: " + difference);
	// Multiplication Operator (*)
	let product = x * y;
	console.log("Result of multiplication operator: " + product);
	// Division Operator (/)
	let quotient = y / x;
	console.log("Result of division operator: " + quotient.toFixed(2));
	// Modulo Operator (%)
	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

// [Section] Assignment Operators
	// Assignment Operator (=)
	// The assignment operator assigns/reassigns the value to the variable
	let assignmentNumber = 8;
	// Addition Assignment Operator (+=)
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable
	assignmentNumber += 2;
	console.log(assignmentNumber);
	assignmentNumber += 3;
	console.log("Result of addition assignment oeprator: " + assignmentNumber);
	// Substraction/Multiplication/Division Assignment Operator (-=, *=, /=)

	// Substraction Assignment Operator
	assignmentNumber -= 2;
	console.log("Result of substraction assignment operator: " + assignmentNumber);
	// Multiplication Assignment Operator
	assignmentNumber *= 3;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);
	// Division Assignment Operator
	assignmentNumber /= 11;
	console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parenthesis
	// MDAS - Multiplication or Division First then Addition or Substraction, from left to right
	let mdas = 1 + 2 - 3 * 4 / 5;
	/*
		1.) 3 * 4 = 12
		2.) 12 / 5 = 2.4
		1 + 2 - 2.4
		3.) 1 + 2 - 2.4 = 0.6
	*/
	console.log(mdas.toFixed(1));

	let pemdas = 1 + (2-3) * (4/5);
	/*
		1.) 4 / 5 = 0.8
		2.) 2 - 3 = -1
		1 + (-1) * (0.8)
		3.) -1 * 0.8 = -0.8
		4.) 1 + -0.8 = 0.2
	*/
	console.log(pemdas.toFixed(1));

// [Section] Increment and Decrement
	// Operators that add or substract values by 1 and reassigns the value of the variable where the increment and decrement applied to.

	let z = 1;
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	increment = z++;
	console.log("The result of post-increment: " + increment);
	console.log("The result of post-increment: " + z);

	x = 1;
	let decrement = --x;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + x);

	decrement = x--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + x);

// [Section] Type Coercion
	/*
		Type Coercion is the automatic or implicit conversion of values from one data type to another.
		This happens when operations are performed on different data types that would normally possible and yield irregular results.
	*/

	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;
	// If you are going to add string and number, it will concatenate its value.
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);
	// The boolean "true" is also associated with the value of 1
	let numE = true + 1;
	console.log(numE);
	let numF = false + 1;
	console.log(numF);

// [Section] Comparison Operators
	let juan = "juan";
	// Equality Operator (==)
	/*
		 Checks whether the operands are equal/have the same content/value.
		 Attempts to convert and compare operands of different data types.
		 Returns a boolean value.
	*/
	console.log( 1 == 1); //true
	console.log( 1 == 2); //false
	console.log(1 == '1'); //true
	console.log(0 == false); //true
	// compare two strings that are the same
	console.log("juan" == "juan"); //true
	console.log("JUAN" == "juan"); //false (case-sensitive)
	console.log(juan == "juan"); //true

	// Inequality Operation (!=)
	/*
		Checks wether the operands are not equal/have different content/values.
		Attempts to convert and compare operands of different data types.
		Returns a boolean value.
	*/
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); //false
	console.log(0 != false); //false
	console.log('JUAN' != 'juan'); //true
	console.log(juan != 'juan'); //false

	// Strict Equality Operator (===)
	/*
		Checks wether the operands are equal/have the same content
		Also compares the data types of two values
	*/
	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === '1') //false
	console.log(0 === false); //false
	console.log('JUAN' === 'juan'); //false
	console.log(juan === 'juan'); //true

	// Stictly Inequality Operator (!==)
	/*
		Checks wether the operands are not equal/have different content.
		Also compares the data types of 2 values.
	*/
	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== '1') //true
	console.log(0 !== false); //true
	console.log('JUAN' !== 'juan'); //true
	console.log(juan !== 'juan'); //false

// [Section] Relational Operators
// Checks whether one value is greater or less than to the other values.
	let a = 50;
	let b = 65;

	// Greater Than Operator (>)
	let isGreaterThan = a > b;
	console.log(isGreaterThan); //false
	// Less Than Operator (<)
	let isLessThan = a < b; 
	console.log(isLessThan); //true
	a = 65;
	// Greater Than or Equal Operator (>=)
	let isGTorEqual = a >= b;
	console.log(isGTorEqual); //false
	// Less Than or Equal Operator (<=)
	let isLTorEqual = a <= b; 
	console.log(isLTorEqual); //true

	let numStr = '30';
	console.log(a > numStr);
	console.log(b <= numStr);

	let strNum = "twenty";
	console.log(b >= strNum);
	// since the string is not numeric, the string was converted to a number and it resulted to NaN (Not a Number)

// [Section] Logical Operators
	
	let isLegalAge = true;
	let isRegistered = false;

	// Logical And Operator (&& - Double Ampersand)
	// isRegistered = true;
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND operator: " +allRequirementsMet);

	// Logical OR Operator (|| - Double Pipe)
	// Returns "true" if one of the operands are true
	// isLegalAge = false;
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " +someRequirementsMet);

	// Logical NOT Operator (! - Exclamation Point)
	// Returns the opposite value.
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT Operator: " + someRequirementsNotMet);