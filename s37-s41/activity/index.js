const express = require('express');
const mongoose = require('mongoose');

// It will allow backend application to be available to our frontend application
// It will also allows us to control the app's Cross Origin Resource sharing settings
const cors = require('cors');
const usersRoutes = require('./routes/usersRoutes.js');
const coursesRoutes = require('./routes/coursesRoutes.js');
const port = 4001;
const app = express();

// MongoDB connection
// Establish the connection between the DB and the application or server
// The name of the database should be: 'CourseBookingAPI'
mongoose.connect('mongodb+srv://admin:admin@batch288matos.rnvh5zx.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority', {useNewUrlParser:true, useUnifiedTopology: true});
const database = mongoose.connection;
database.on('error', console.error.bind(console, "Error, can't connect to the database!"))
database.once('open', () => console.log('Connected to the database!'))


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Reminder that we are going to use this for the sake of the bootcamp
app.use(cors());

// Add the routing of the routes from the usersRoutes
app.use('/users', usersRoutes);
app.use('/courses', coursesRoutes);


if(require.main === module){
	app.listen(process.env.PORT || 4000, () => {
	    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
	});
}