// Advanced Queries

// Query an embedded document/object
db.users.find({
	contact: {phone: "87654321", email: "stephenhawking@gmail.com"}
});

db.users.find({
	contact: {email: "stephenhawking@gmail.com"}
});

// Dot notation
db.users.find({
	"contact.email": "stephenhawking@gmail.com"
});

// Querying an array with exact element
db.users.find({
	courses: ["CSS", "Javascript", "Python"]
});

db.users.find({
	courses: {$all: ["React", "Laravel"]}
});

db.users.find({
	courses: {$all: ["React", "Sass"]}
});

// Query Operators

// [Section] Comparison Query Operators
	// $gt/$gte operator
		/*
			- allows us to find documents that have field number values greater or equal to a specified value
			- Note that this operator will only work if the data type of the field is number or integer
			Syntax:
				db.collectionName.find({field: {
					$gt/$gte: value
				}})
		*/

db.users.find({
	age: {$gt: 76}
});

db.users.find({
	age: {$gte: 76}
});
	
	// $lt/$lte operator
		/*
			- allows us to find documents that have field number value less than or equal to a specified value
			- Note: same with $gt/$gte operator, this will only work if the data type of the field being queried is number or integer
			Syntax:
				db.collectionName.find({find: {
					$lt/$lte: value
				}})
		*/

db.users.find({
	age: {$lt: 76}
});

db.users.find({
	age: {$lte: 76}
});

	// $ne Operator
		/*
			- allows us to find documents that have field number values not equal to specified value
			Syntax:
				db.collectionName.find({field: {
					$ne: value
				}})
		*/
db.users.find({
	age: {$ne: 76}
});

	// $in Operator
		/*
			- allows us to find documents with specific match criteria one field using different values
			Syntax:
				db.collectionName.find({field: {
					$in: value
				}})
		*/
db.users.find({
	lastName: {$in: ["Hawking", "Doe", "Armstrong"]}
});

db.users.find({
	"contact.phone": {$in: ["87654321"]}
});

// Using $in operator in an array
db.users.find({
	courses: {$in: ["React"]}
});

db.users.find({
	courses: {$in: ["React", "Laravel"]}
});

// [Section] Logical Query Operators
	// $or operator
	/*
		- allows us to find documents that match a single criteria from multiple provided search criteria
		Syntax:
			db.collectionName.find({
				$or: [{fieldA: valueA}, {fieldB: valueB}]
			});
	*/

db.users.find({
	$or:[{firstName: "Neil"}, {age: 25}]
});
//  add multiple operator
db.users.find({
	$or: [{firstName: "Neil"}, {age: {$gt: 25}}]
});

db.users.find({
	$or: [{firstName: "Neil"}, {age: {$gt: 25}}, {courses: {$in: ["CSS"]}}]
});

// $and Operator
/*
	- allows us to find documents matching all the multiple criteria in a single field.
	Syntax:
		db.collectionName.find({
			$and: [{fieldA: valueA}, {fieldB: valueB}], . . .
		});
*/

db.users.find({
	$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]
});

// Mini-activity
	// You are going to query all the documents from the users collection, that follow these criteria:
		// Information of the documents older than 30 that is enrolled in CSS or HTML

db.users.find({
	$and: [{age: {$lt: 30}}, {courses: {$in: ["CSS", "HTML"]}}]
});

// [Section] Field Projection
	/*
		- retreiving documents are common operations that we do and by default mongoDB queries return the whole document as a response
	*/

	// Inclusion
	/*
		- allows us to include or add specific fields only when retreiving documents:
		Syntax:
			db.collectionName.find({
				criteria, {field: 1}
			});
	*/

db.users.find({
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	"contact.phone": 1,
	_id: 0
});

db.users.find({
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1
});

// Exclusion
/*
	- allows us to exclude/remove specific fields only when retrieving documents
	Syntax:
		db.users.find({
			criteria
		}, 
		{
			field: 0
		}
		),
*/

db.users.find({
	firstName: "Jane"
},
{
	contact: 0,
	department: 0
});

db.users.find({
	firstName: "Jane"
},
{
	"contact.email": 0,
	department: 0
});

// Evaluation Query Operator
	// $regex operator
		/*
			- allow us to find documents that match a specific string pattern using regular expressions.
			Syntax:
				db.collectionName.find({
					field: $regex: 'pattern', option: 'optionvalue'
				});
		*/
// Case sensitive
db.users.find({
	firstName: {$regex: "N"}
});

db.users.find({
	firstName: {$regex: "N", $options: "i"}
});
