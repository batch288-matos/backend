// Single Line Comment
/*
	Multi Line Comment
	This is a comment
*/

// [Section] Syntax, Statements and Comments
// Statements in programming, these are the instructions that we tell the computer to perform
// JavaScript statement usually it ends with semicolo(;)
// Semicolons are not required in JS, but we will use it to help us prepare for the other strict language like Java.
// A Syntax in programming, it is the set of rules that describes how statement must be constructed
// All lines/blocks of code should be written in a specific or else the statement will not run

// [Section] Variables
// It is used to contain/store data.
// Any information that is used by an application is stored in what we call memory.
// When we create variables, certain portion of the device memory is given a "name" that we call "variables"

// Declaring Variables
// Declaring varibles - tells our devices that a variable name is created and is ready to store data.
	// Syntax:
		// let/const variableName

let myVariable;

// by default if you declare a variable and did not initialize its value it will become "undefined"

// console.log() is useful for printing values of a variable or certain results of code into Google Chromes Browser's console.
console.log(myVariable);

/*
	Guides in writing variables:
		1. Use the 'let' keyword followed by the variable name of your choice and use the assignment operator (=) to assign a value.
		2. Variable names should start with a lowercase character, use camelCase for multiple words.
		3. For constant variables, use the 'const' keyword.
		4. Variable names, it should be indicative (descriptive) of the value being store to avoid confusion.
*/

// Declaring and initializing variables
// Initializing variables - the instance when a variable is given it's initial or starting value.
	// Syntax
		// let/const variableName = value;

// example:
let productName = 'desktop computer';

console.log(productName)

let productPrice = 18999;
console.log(productPrice)

// In the context of certain applications, some variables/information are constant and should not change
// In this example, the interest rate for a loan or savings account or a mortgage must not change due to real world concerns.

const interest = 3.539;

// Reassigning Variable values
// Reassigning a variable, it means changing it's initial or previous value into another value.
	// Syntax
	// variableName = newValue

productName = 'Laptop';
console.log(productName);

// The value of a variable declared using the const keyword can't be reassigned.

/*interest = 4.489;
console.log(interest);*/

// Reassigning variables vs. Initializing variables
// Declares a variable 

let supplier;
// Initializing
supplier = "John Smith Tradings";
// Reassigning
supplier = "Uy's Trading";
// Declaring and initializing a variable
let consumer = "Chris";
// Reassigning
consumer = "Topher"

// Can you declare a const variable without initialization

/*const driver;

driver = "Warlon Jay"*/

// var vs. let/const keyword
	// var - is also used in declaring variables but var is an EcmaScript 1 version (1997)
	// let/const keyword was introduced as a new feature in ES6 (2015)

// What makes let/const different from var?
	// There are issues associated with variables declared or created using var, regarding hoisting
	// Hoisting is JavaScript default behavior of moving declaration to the top.
	// In Terms of variables and constants, keyword var is hoisted and let and const does not hoisting.

	// Example of Hoisted:

	a = 5;
	console.log(a);

	var a;

	/*b = 6;
	console.log(b);
	let b;*/

// let/const local/global scope
	// Scope essentially means where these variables are available or accessible for use

	// let and const are block scoped
	// A block is a chunk of code bounded by {}. A block lives in a curly braces. Anything within the braces are block.

let outerVariable = "hello";

let globalVariable;

	{
		let innerVariable =	"hello again";

		console.log(innerVariable);
		console.log(outerVariable);

		globalVariable = innerVariable;
	}

	// console.log(innerVariable);

	console.log(globalVariable);

// Multiple variable declarations and initialization
// Multiple variables may be declared in one statement

let productCode = "DC017", productBrand = "Dell";

console.log(productCode);
console.log(productBrand);

// Multiple variables to be consoled in one line.
console.log(productCode, productBrand);

// Using a variable with a reversed keyword.
// Reserved keywords cannot be used as a variable name as it has function in JavaScript.
/*const let = "Hi Im let keyword"

console.log(let);*/

// [Section] Data Types

// Strings
// Strings are a series of characters that create a word, a phrase, a sentence or anything to creating text.
// String in Javascript can be written using either a single quote ('')or double quote ("")

let country = "Philippines";
let province = "Metro Manila";
	// Contenation of strings in JavaScript
		// Multiple string values can be combines to create a single string using the "+" symbol

let fullAddress = province + ',' + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// The escape characters (\) in strings in combination with other characters can produce different effects/results. 
// "\n" this creates a next line in between text.

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);
		
let message = "John's employess went home early.";
console.log(message);

message = 'John\'s employess went home early.';
console.log(message);

// Numbers
// Integers/whole numbers

let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// Exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and number
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to create values relating to the state of certain things.
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Arrays
// Arrays are special kind of data that's used to store multiple related values.
// In JavaScript, Arrays can store different data types but is normally used to store similar data types.

// similar data types
// Syntax
	// let/const arrayName = [elementA, elementB, elementC, ...]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types / not recommended in using array
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that is used to mimic real world object/items.
/*
	Syntax
	let/const objectName = {
		propertyA: valueA,
		propertyB: valueB,
	}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["+63917 123 4567", "8123 4567"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}

console.log(person);

// typeof operator is used to determine the type of data or value of a variable. It outputs string

console.log(typeof mailAddress);
console.log(typeof headcount);
console.log(typeof isMarried);

console.log(typeof grades);

// Note: Array is a special type of object with methods and functions to manipulate.

// Constant Objects and Arrays
/*
The keywords const is a little misleading

It does not define a constant value. It defines a constant reference to a value:

Because of this you can not:
Reassign a constant value.
Reassign a constant array.
Reassign a constant object.

But you can:

Change the elements of a constant array.
Change the properties of constant object.
*/

const anime = ["One Piece", "One Punch Man", "Attack on Titan"];

/*anime = ["One Piece", "One Punch Man", "Kimetsu no Yaiba"];

console.log(anime);*/

anime[2] = "Kimetsu no Yaiba";

console.log(anime);
/*console.log(anime[2]);*/

// Null
// It is used to intentionally express the absence of a value in a variable.

let spouse = null;

spouse = "Maria";

// Undefined
// Represents the state of a variable that has been declared but without an assigned value.

let fullName;

fullName = "Maria";
