// console.log("HELLO!")

// Array Methods
	// JavaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items.
	
	// Mutator Methods
	// Mutator methods are functions that "mutate" or change an array after they are created.
	// These methods manipulat the original array performing various tasks such as adding or removing elements.


	let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

	// push()
		/*
			- Adds an element in the end of an array and returns the array's length
			Syntax:
				arrayName.push();
		*/

	console.log("Current array: ");
	console.log(fruits);

	let fruitsLength = fruits.push("Mango");
	console.log(fruitsLength);
	console.log("Mutated array from push method: ");
	console.log(fruits);

	// Pushing multiple elements to an array

	fruitsLength = fruits.push("Avocado", "Guava");
	console.log(fruitsLength);
	console.log("Mutated array after pushing multiple elements: ");
	console.log(fruits);

	// pop ()
	/*
		- removes the last element and returns the removed element
		Syntax:
			arrayName.pop();
	*/

	console.log("Current Array: ");
	console.log(fruits);

	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log("Mutated array from the pop method: ");
	console.log(fruits);

	// unshift()
	/*
		- it adds one or more elements at the beginning of an array and it returns the update array length
		Syntax:
			arrayName.unshift("elementA");
			arrayName.unshift("elementA", "elementB", "elementC" . . .);
	*/

	console.log("Current Array: ");
	console.log(fruits);

	fruitsLength = fruits.unshift("Lime", "Banana");
	console.log(fruitsLength);
	console.log("Mutated array from unshift method:");
	console.log(fruits);

	// shift()
	/*
		- removes an element at the beginning of an array and returns the removed element
		Syntax:
			arrayName.shift();
	*/

	console.log("Current Array:");
	console.log(fruits);

	removedFruit = fruits.shift();
	console.log(removedFruit);
	console.log("Mutated array from shift method:");
	console.log(fruits);

	// splice()
	/*
		- Simultaneously removes an elements from a specified index number and adds element
		Syntax:
			arrayName.splice(startingIndex, deleteCount, elementstoBeAdded);
	*/

	console.log("Current Array:");
	console.log(fruits);

	fruits.splice(fruits.length, 0, "Cherry");
	console.log("Mutated array after the splice method:");
	console.log(fruits);

	// sort()
	/*
		- Rearranges the array elements in alphanumeric order
		Syntax:
			arrayName.sort();
	*/

	console.log("Current Array:");
	console.log(fruits);

	fruits.sort();

	console.log("Mutated array after sort method:");
	console.log(fruits);

	// reverse()
	/*
		- reverses the order of array elements
		Syntax:
			arrayName.reverse();
	*/

	console.log("Current Array:");
	console.log(fruits);

	fruits.reverse();

	console.log("Mutated array after sort method:");
	console.log(fruits);

// [Section] Non-mutator methods
/*
	- Non-mutator methods are functions that do not modify or change an array after they're created 
	- These methods do not manipulate the original array performing various task such as returning elements from an array and combining arrays and printing the output
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

	// indexOf()
	/*
		- returns the index number of the first matching element found in an array.
		- if no match was found, the result will be -1
		- The search process will be done from the first element proceeding to the last element

		Syntax:
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, startingIndex);
	*/

	let firstIndex = countries.indexOf("PH");
	console.log(firstIndex);

	let invalidCountry = countries.indexOf("BR");
	console.log(invalidCountry);

	firstIndex = countries.indexOf("PH", 2);
	console.log(firstIndex);

	console.log(countries);

	// lastIndexOf()
	/*
		- returns the index number of the last matching element found in an array
		-  the search process will be done from last element proceeding to the first element
		Syntax: 
			arrayName.lastIndexOf("searchValue");
			arrayName.lastIndexOf("searchValue", startingIndex);
	*/

	let lastIndex = countries.lastIndexOf("PH");
	console.log(lastIndex);

	invalidCountry = countries.lastIndexOf("BR");
	console.log(invalidCountry);

	lastIndex = countries.lastIndexOf("PH", 4);
	console.log(lastIndex);

	// indexOf, starting from the starting index going to the last element(from left to right)
	// lastIndexOf, statrting from the last element going to the first element(from right to left)

	console.log(countries);

	// slice()
	/*
		- portion/slices elements from an array and retur a new array
		Syntax: 
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex, endingIndex);
	*/

	let slicedArrayA = countries.slice(2);
	console.log("Result from slice method:");
	console.log(slicedArrayA);

	// slicing off elements from a specified index to another index:
	let slicedArrayB = countries.slice(2, 4);
	console.log("Result fro slice method:");
	console.log(slicedArrayB);

	// slicing off elements starting from the last element of an array

	let sliceArrayC = countries.slice(-5);
	console.log("Result from slice method:");
	console.log(sliceArrayC);

	// toString()
	/*
		- returns an array as string separated by commas
		Syntax:
			arrayName.toString();
	*/

	let stringArray = countries.toString();
	console.log("Result from toString method:");
	console.log(stringArray);
	console.log(typeof stringArray);

	// concat()
	/*
		- combines arrays to an array or elements and returns the combined result.
		Syntax:
			arrayA.concat(arrayB);
			arrayA.contact(elementA);
	*/

	let tasksArrayA = ["drink HTML", "eat javascript"];
	let tasksArrayB = ["inhale CSS", "breathe sass"];
	let tasksArrayC = ["get git", "be node"];

	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log(tasks);

	let combineTasks = tasksArrayA.concat("smell express", "throw react");
	console.log(combineTasks);

	let combineTasksA = tasksArrayA.concat(tasksArrayB[1]);
	console.log(combineTasksA);

	// concat multiple array into an array

	let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
	console.log(allTasks);

	// contact array to an array and element
	let exampleTasks = tasksArrayA.concat(tasksArrayB, "smell express");
	console.log(exampleTasks);

	// join()
	/*
		- returns an array as string separated by specified separator string
		Syntax:
			arrayName.join("separatorString");
	*/

	let users = ["John", "Jane", "Joe", "Robert"];
	console.log(users.join());
	console.log(users.join(""));
	console.log(users.join(" - "));

// [Section] Iteration Methods
	/*
		- Iteration methods are loops designed to perform repetitive task
		- Iteration methods loops over all items in an array
	*/

	// forEach()
	// Similar to a for loop that iterates on each of array element.
	/*
		Syntax:
			arrayName.forEach(function(indivElement{statement};);
	*/

	console.log(allTasks);

	allTasks.forEach(function(task){
		console.log(task);
	});

	// filteredTask variable will hold all the elements from the allTasks array that has more than 10 characters.

	let filteredTasks = [];

	/*allTasks.forEach(function(task){
		if(task.length > 10){
			filteredTasks.push(task)
		}
	});
	console.log(filteredTasks);*/

	allTasks.forEach(function(task){
			if(task.length > 10){
				filteredTasks.push(task);
			}
		});
		console.log(filteredTasks);


	// map()
	// Iterates on each element and returns new array with diffrenet values depending on the result of the function's operation
	let numbers = [1, 2, 3, 4, 5];

		let numberMap = numbers.map(function(number){
			
			return number * 3;
		})
		console.log(numbers);
		console.log(numberMap);

	// every()
	/*
		- it will check if all elements in an array meet the given condition
		- it will return true value if all elements meet the condition and false otherwise
		Syntax:
			let/const resultArray = arrayName.every(function(indivElement){
				return expression/condition;
			});
	*/

	numbers = [1, 2, 3, 4, 5];

	let allValid = numbers.every(function(number){
		return (number<6);
	});

	console.log(allValid);

	// some()
	/*
		- checks if at least one element in the arrays meets the given condition
		Syntax:
			let/const resultArray = arrayName.sum(function(indivElement){
				return expression/condition;
			});
	*/

	/*let someValid = numbers.sum(function(number){
		return (number<3);
	});
	console.log(someValid);*/

	let someValid = numbers.some(function(number){
			return (number < 2);
		})

		console.log(someValid);

	// filter()
	/*
		- returns new array that contains elements which meets the given
		- returns an empty array if no elements were found
		Syntax: 
			let/const resultArray = arrayName.filter(function(indivElement){
				return expression/condition;
			});
	*/

	numbers = [1, 2, 3, 4, 5];

	let filteredValid = numbers.filter(function(number){
		return (number % 2 === 0);
	})

	console.log(filteredValid);

	// includes()
	/*
		- checks if the argument passed can be found in the array
	*/

	let products = ["mouse", "keyboard", "laptop", "monitor"];

	let productFound1 = products.includes("mouse");
	console.log(productFound1);

	// reduce()
	/*
		- evaluates elements from left to right and returns/reduces the array into single value
	*/
	// The first parameter in the function will be accumulatro
	// the second parameter in the function will be the currentValue

	let reduceArray = numbers.reduce(function(x, y){
		console.log("Accumulator " + x);
		console.log("currentValue " + y);
		return x+y;
	});
	
	console.log(reduceArray);

	let sales = [190, 12387, 12381283, 1298312, 1928312];


	products = ["Mouse", 'Keyboard', 'Laptop', 'Monitor'];

	let reducedArray = products.reduce(function(x, y){
		return (x+y);
	})

	console.log(reducedArray)
