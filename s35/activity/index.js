const express = require('express');
// Mongoose is a package that allows creation of schemas to model our data structures
// It also has access to a number of methods for manipulating our database
const mongoose = require('mongoose');

const app = express();

const port = 3001;

	// Section: MongoDB Connection
	// Connect to the database by passing your connection string
	// Due to update in MongoDB drivers that allow connection, the default connection string is being flagged as an arror
	// By default a warning will be displayed in the terminal when the application is running
	// {newUrlParser:true}

	// Syntax:
	// mongoose.connect('MongoDB string', {useNewUrlParser:true});
	mongoose.connect('mongodb+srv://admin:admin@batch288matos.rnvh5zx.mongodb.net/batch288-todo?retryWrites=true&w=majority', {useNewUrlParser:true});

	// Notification whether we connected properly with the database
	let db = mongoose.connection;
	// for catching the error just in case we had an error during the connection
	// console.error.bind allows us to print errors in the browser and in the terminal
	db.on('error', console.error.bind(console, "Error! Can't connect to the Database"));
	// If the connection is successful:
	db.once('open', () => console.log("We're connected to the cloud database!"));

	// Middlewares
	// It allows our app to read json data
	app.use(express.json());
	// It allows our app to read data from forms
	app.use(express.urlencoded({extended:true}));

	// [Section] Mongoose Schemas
	// Schemas determined the structure of the documents to be written in the database
	// Schemas act as blueprints to our database
	// Use the Schema() constructor of the mongoose module to create a new Schema object
	const taskSchema = new mongoose.Schema({
		// Define the fields with the corresponding data type
		name: String,
		// let's add another field which is status
		status: {
			type: String,
			default: 'pending'
		}
	});

	// [Section] Models
	// Uses schema and are used to create/instantiate objects that corresponds to the schema
	// Models use Schema and they act as the middleman from the server (JS code) to our database

	// To create a model we are going to use the model();

	const Task = mongoose.model('Task', taskSchema);

	// [Section] Routes
	// Create a POST route to create a new task
	// Create a new task
	// Business logic:
		// 1. Add a functionality to check whether there are duplicate task
			// if the task is existing in the database, we return an error
			// if the task doesn't exist in the database, we add it in the database
		// 2. The task data will be coming from the request's body

	app.post('/tasks', (request, response) => {
		// findOne method is a mongoose method acts similar to "find" in MongoDB
		// if the findOne method finds a document that matches the criteria it will return the object/document and there's no object that matches the criteria it will return an empty object
		Task.findOne({
			name: request.body.name
		})
		.then(result => {
			// We can use if statement to check or verify whether we have an object found
			if(result !== null){
				return response.send('Duplicate task found!');
			}
			else{
				// Create a new task and save it to the database
				let newTask = new Task({
					name: request.body.name
				});
				// The save() method will store the information to the database
				// Since the newTask was created from the Mongoose Scheme and Task Model it will be saved in tasks collection
				newTask.save();

				return response.send('New task created!');
			};
		});
	});

	// Get all the tasks in our collection
	// 1. Retrieve all the documents
	// 2. If an error is encountered, print the error
	// 3. If no error/s is/are found, send a success status to the client and show the documents retrieved


	app.get('/tasks', (request, response) => {
		// find() method is a mongoose method that is similar to MongoDb find
		Task.find({})
		.then(result => {
			return response.send(result);
		})
		.catch(error => {
			return response.send(error);
		})
	});

	const userSchema = new mongoose.Schema({
		username: String,
		password: String
	});

	const User = mongoose.model('User', userSchema);

	app.post('/signup', (request, response) => {
		User.findOne({
			username: request.body.username
		})
		.then(result => {
			if(result !== null){
				return response.send('Duplicate username found')
			}
			else{
				if(request.body.username !== "" && request.body.password !== ""){
					let newUser = new User({
						username: request.body.username,
						password: request.body.password
					})

					newUser.save()
					.catch(error  => {
						return response.send(error);
					});
					
					return response.send(201, 'New user registered');
				}
				else{
					return response.send('BOTH username and password must be provided');
				};
			};
		});
	});


if(require.main === module){
	app.listen(port, () => {console.log(`Server is running at port ${port}!`)});
}

module.exports = app;
