const express = require('express');

const taskControllers = require('../controllers/taskControllers.js')
// Contain all the endpoints of our application
const router = express.Router();

router.get('/', taskControllers.getAllTasks);
router.post('/addTask', taskControllers.addTasks);

// Parameterized

// We are going to create a route using Delete method at the URL '/tasks/:id'
// The colon here is an identifier that helps create a dynamic route which allows us to supply information
router.delete('/:id', taskControllers.deleteTask);
router.get('/:id', taskControllers.getTask);
router.put('/:id/complete', taskControllers.updateTask);
module.exports = router; 