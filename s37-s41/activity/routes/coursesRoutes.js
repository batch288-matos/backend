const express = require('express');
const coursesControllers = require('../controllers/coursesControllers.js');
const auth = require('../auth.js');

const router = express.Router();

// routers without params
// This route is responsible for adding course in our database.
router.post('/addCourse', auth.verify, coursesControllers.addCourse);

// This route is for retreiving all courses
router.get('/', auth.verify, coursesControllers.getAllCourses);

// This route is for retrieve all active courses
router.get('/activeCourses', coursesControllers.getActiveCourses);

// Route for inactive courses
router.get('/inactiveCourses', auth.verify, coursesControllers.getInactiveCourses);

// routers with params
// This route is for retreiving specific course information
router.get('/:courseId', coursesControllers.getCourse);

// This route for updating course
router.patch('/:courseId', auth.verify, coursesControllers.updateCourse);

// This route for archiving course
router.patch('/:courseId/archive', auth.verify, coursesControllers.archiveCourse);


module.exports = router;