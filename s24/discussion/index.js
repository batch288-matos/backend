// console.log("Hello")

// [Section] Exponent Operator
	// Before the ES6 update
	// Math.pow(base, exponent);

	// 8 raise to the power of 2
	const firstNum = Math.pow(8, 2);
	console.log(firstNum);
	const secondNum = Math.pow(5, 5);
	console.log(secondNum);

	// After the ES6 update
	const thirdNum = 8 ** 2;
	console.log(thirdNum);
	const fourthNum = 5 ** 5;
	console.log(fourthNum);

// [Section] Template Literals
	// It will allow us to write strings without using the concatenation operator
	// Greatly helps with the code readability

	// Before ES6 Update
	let name = "John";
	let message = "Hello " + name + "! Welcome to programming!";
	console.log(message);

	// After the ES6 Update
	// Uses backticks(``)

	message = `Hello ${name}! Welcome to programming!`;
	console.log(message);
	console.log(typeof message);

	// Multi-line using Template literals

	const anotherMessage = `${name} attended a match competition.
	He won it by solving the problem 8**2 with the
	solution of ${firstNum}`;
	console.log(anotherMessage);

	const interestRate = .1;
	const principal = 1000;
	// Template literals allows us to write strings with embedded JavaScript expressions
	// expressions are any valid unit of code that resolved to a value
	// "${}" are used to include JavaScript expressions in strings using the template literals
	console.log(`The interest on your savings account is ${interestRate * principal}`);

// [Section] Array Destructuring
	/*
		- allows us to unpack elements in array into distinct variables
		Syntax:
			let/const [variableNameA, variableNameB, variableNameC, . . .] = arrayName;
	*/

	const fullName = ["Juan", "Dela", "Cruz"];

	// Before the ES6 Update
	let firstName = fullName[0];
	let middleName = fullName[1];
	let lastName = fullName[2];
	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

	// After the ES6 Update

	// const [givenName, secondName, familyName] = fullName;
	// console.log(givenName);
	// console.log(secondName);
	// console.log(familyName);
	// This one will cause an error simply because we use the const keyword in array destructuring
	/*givenName = "Pedro";
	console.log(givenName);*/

	// Another example of Array Destructuring
	let gradesPerQuarter = [98, 97, 95, 94]
	console.log(gradesPerQuarter);
	let [firstGrading, secondGrading, thirdGrading, fourthGrading] = gradesPerQuarter;
	console.log(firstGrading);
	console.log(secondGrading);
	console.log(thirdGrading);
	console.log(fourthGrading);
	// Unlike the const keyword, this won't cause any error
	firstGrading = 100;
	console.log(firstGrading)

// [Section] Object Destructuring
	// allows us to unpack properties of objects into distinct variables
	// shortens the syntax for accessing properties from objects
	/*
		Syntax:
			let/const {propertyNameA, propertyNameB, . . .} = objectName;
	*/

	// Before the ES6 Update
	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"
	};
	console.log(person);
	// const givenName = person.givenName;
	// const maidenName = person.maidenName;
	// const familyName = person.familyName;
	// console.log(givenName);
	// console.log(maidenName);
	// console.log(familyName);


	// After the ES6 Update
	// The order of the property/variable does not affect its value
	const {maidenName, familyName, givenName} = person;
	console.log(`This is the given name ${givenName}`);
	console.log(`This is the given name ${maidenName}`);
	console.log(`This is the given name ${familyName}`);

// [Section] Arrow function
	// Compact alternative syntax to a traditional functions
	/*
		Syntax:
			const/let variableName = () => {
				statement/codeblock
			}
	*/
	// Arrow function without parameter 
	const hello = () => {
		console.log("Hello World from the arrow function");
	}

	hello();

	// Arrow function with parameter
	/*
		Syntax:
			const/let variableName = (parameter) => {
				statement/codeblock
			}
	*/

	const printFullName = (firstName, middleInitial, lastName) => {
		console.log(`${firstName} ${middleInitial} ${lastName}`);
	};

	printFullName("John", "D", "Smith");

	// Arrow function can also be used with loops
	// Example
	const students = ["John", "Jane", "Judy"];

	students.forEach((student) => {
		console.log(`${student} is a student.`);
	});

// [Section] Implicit return in Arrow function
	// Example:
		// if the function will run one line or one statment the arrow function will implicit return the value
		const add = (x, y) => x+y;
	 	let total = add(10, 12);
	 	console.log(total);

	 	function trialFunction(x, y){
	 			return x+y;
		};
	 	console.log(trialFunction(1,2));

// [Section] Default function Argument value
	// provides a default function argument value if none is provided when the function is invoked

	const greet = (name = "user", age = 0) => {
		return `Good morning, ${name}! I am ${age} years old`;
	}
	console.log(greet(undefined, 78));

	function addNumber(x = 0, y = 0){
		console.log(x);
		console.log(y);
		return x + y;
	};

	let sum = addNumber(y=1);
	console.log(sum);

// [Section] Class-Based Object Blueprints
	// Allow us to create/instantiate of objects using a class as blueprint
	/*
		Syntax:
			class className{
					constructor(objectPropertyA, objectPropertyB){
						this.objectPropertyA = objectValueA;
						this.objectPropertyB = objectvalueB;
					};
			};
	*/

	class Car {
		constructor(brand, name, year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		};
	};

	// Instantiate an Object

	const myCar = new Car("Ford", "Ranger Raptor", 2021);
	console.log(myCar);